package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IReceiverService;
import com.t1.yd.tm.listener.LoggerListener;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jms.*;

@Service
public class ReceiverService implements IReceiverService {

    @NotNull
    private final ConnectionFactory connectionFactory;

    @NotNull
    private final LoggerListener loggerListener;

    @Autowired
    public ReceiverService(@NotNull final LoggerListener loggerListener,
                           @NotNull final ConnectionFactory connectionFactory) {
        this.loggerListener = loggerListener;
        this.connectionFactory = connectionFactory;
    }

    @Override
    @SneakyThrows
    public void receive() {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createQueue("LOG_QUEUE");
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(loggerListener);
    }

}
