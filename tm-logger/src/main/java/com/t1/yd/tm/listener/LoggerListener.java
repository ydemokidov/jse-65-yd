package com.t1.yd.tm.listener;

import com.t1.yd.tm.api.service.ILoggerService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public class LoggerListener implements MessageListener {

    @NotNull
    private final ILoggerService loggerService;

    @Autowired
    public LoggerListener(@NotNull final ILoggerService loggerService) {
        this.loggerService = loggerService;
    }

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (!(message instanceof TextMessage)) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        loggerService.log(textMessage.getText());
    }

}
