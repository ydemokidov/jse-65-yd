package com.t1.yd.tm.api.service;

import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskService {

    @NotNull
    Task add(@NotNull Task task) throws Exception;

    void clear() throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @NotNull
    List<Task> findAll() throws Exception;

    @Nullable
    Task findOneById(@Nullable String id) throws Exception;

    int count() throws Exception;

    void remove(@Nullable Task task) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    void update(@Nullable Task task) throws Exception;

    void changeTaskStatusById(@Nullable String id, @Nullable Status status) throws Exception;

    @NotNull
    Task create(@Nullable String name) throws Exception;

    @NotNull
    Task create(@Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    List<Task> findAllByProjectId(@Nullable String projectId) throws Exception;

    void updateById(@Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    void updateProjectIdById(@Nullable final String id, @Nullable final String projectId) throws Exception;

}
