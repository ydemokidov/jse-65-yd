package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.ITaskDTOService;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.exception.field.DescriptionEmptyException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.NameEmptyException;
import com.t1.yd.tm.repository.TaskDTORepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
public class TaskDTOService implements ITaskDTOService {

    @NotNull
    private final TaskDTORepository repository;

    @Autowired
    public TaskDTOService(@NotNull final TaskDTORepository repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO add(@NotNull final TaskDTO task) throws Exception {
        return repository.save(task);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public @NotNull List<TaskDTO> findAll() throws Exception {
        return repository.findAll();
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Override
    public int count() throws Exception {
        return (int) repository.count();
    }

    @Override
    @Transactional
    public void remove(@Nullable final TaskDTO task) throws Exception {
        if (task == null) return;
        repository.delete(task);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public TaskDTO update(@Nullable final TaskDTO task) throws Exception {
        if (task == null) return null;
        return repository.save(task);
    }

    @Override
    @Transactional
    public void changeTaskStatusById(@Nullable final String id, @Nullable final Status status) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        repository.save(task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setName(name);
        return repository.save(task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@Nullable final String name, @Nullable final String description) throws Exception {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        return repository.save(task);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findByProjectId(projectId);
    }

    @Override
    @Transactional
    public void updateById(@Nullable final String id, @Nullable final String name, @Nullable final String description) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
    }

    @Override
    @Transactional
    public void updateProjectIdById(@Nullable final String id, @Nullable final String projectId) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        repository.save(task);
    }

}
