package com.t1.yd.tm.api.service;

import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskDTOService {

    @NotNull
    TaskDTO add(@NotNull TaskDTO task) throws Exception;

    void clear() throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @NotNull
    List<TaskDTO> findAll() throws Exception;

    @Nullable
    TaskDTO findOneById(@Nullable String id) throws Exception;

    int count() throws Exception;

    void remove(@Nullable TaskDTO task) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    @Nullable
    TaskDTO update(@Nullable TaskDTO task) throws Exception;

    void changeTaskStatusById(@Nullable String id, @Nullable Status status) throws Exception;

    @NotNull
    TaskDTO create(@Nullable String name) throws Exception;

    @NotNull
    TaskDTO create(@Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable String projectId) throws Exception;

    void updateById(@Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    void updateProjectIdById(@Nullable final String id, @Nullable final String projectId) throws Exception;

}
