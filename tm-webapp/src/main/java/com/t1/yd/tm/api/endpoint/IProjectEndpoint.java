package com.t1.yd.tm.api.endpoint;

import com.t1.yd.tm.dto.model.ProjectDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectEndpoint {

    @NotNull
    List<ProjectDTO> findAll() throws Exception;

    @NotNull
    ProjectDTO save(@NotNull ProjectDTO project) throws Exception;

    @Nullable
    ProjectDTO findById(@NotNull String id) throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    long count() throws Exception;

    void deleteById(@NotNull String id) throws Exception;

    void delete(@NotNull ProjectDTO project) throws Exception;

    void clear() throws Exception;

}
