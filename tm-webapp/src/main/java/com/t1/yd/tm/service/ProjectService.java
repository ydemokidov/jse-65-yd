package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IProjectService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.exception.field.DescriptionEmptyException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.NameEmptyException;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @NotNull
    private ProjectRepository repository;

    @NotNull
    @Override
    @Transactional
    public Project add(@NotNull final Project project) throws Exception {
        return repository.save(project);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public List<Project> findAll() throws Exception {
        return repository.findAll();
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        return repository.findById(id).orElse(null);
    }

    @Override
    public int count() throws Exception {
        return (int) repository.count();
    }

    @Override
    @Transactional
    public void remove(@Nullable final Project project) throws Exception {
        if (project == null) return;
        repository.delete(project);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void update(@Nullable final Project project) throws Exception {
        if (project == null) return;
        repository.save(project);
    }

    @Override
    @Transactional
    public void changeProjectStatusById(@Nullable final String id, @Nullable final Status status) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        repository.save(project);
    }

    @NotNull
    @Override
    @Transactional
    public Project create(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        return repository.save(project);
    }

    @NotNull
    @Override
    @Transactional
    public Project create(@Nullable final String name, @Nullable final String description) throws Exception {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return repository.save(project);
    }

    @Override
    @Transactional
    public void updateById(@Nullable final String id, @Nullable final String name, @Nullable final String description) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
    }


}
