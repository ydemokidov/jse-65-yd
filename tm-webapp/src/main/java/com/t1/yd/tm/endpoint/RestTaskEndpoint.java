package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.api.service.ITaskDTOService;
import com.t1.yd.tm.dto.model.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/task")
public class RestTaskEndpoint implements ITaskEndpoint {

    private final ITaskDTOService taskDTOService;

    @Autowired
    public RestTaskEndpoint(ITaskDTOService taskDTOService) {
        this.taskDTOService = taskDTOService;
    }

    @Override
    @GetMapping("/list")
    public @NotNull List<TaskDTO> findAll() throws Exception {
        return taskDTOService.findAll();
    }

    @Override
    @PostMapping("/update")
    public @NotNull TaskDTO save(@RequestBody @NotNull final TaskDTO task) throws Exception {
        return taskDTOService.update(task);
    }

    @Override
    @GetMapping("/{id}")
    public @Nullable TaskDTO findById(@PathVariable @NotNull final String id) throws Exception {
        return taskDTOService.findOneById(id);
    }

    @Override
    @GetMapping("/exists/{id}")
    public boolean existsById(@PathVariable @NotNull final String id) throws Exception {
        return taskDTOService.existsById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() throws Exception {
        return taskDTOService.count();
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable @NotNull final String id) throws Exception {
        taskDTOService.removeById(id);
    }

    @Override
    @DeleteMapping("/delete")
    public void delete(@RequestBody @NotNull final TaskDTO task) throws Exception {
        taskDTOService.remove(task);
    }

    @Override
    public void clear() throws Exception {
        taskDTOService.clear();
    }

}
