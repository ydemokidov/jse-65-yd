package com.t1.yd.tm.controller;

import com.t1.yd.tm.api.service.IProjectService;
import com.t1.yd.tm.api.service.ITaskService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/task")
public class TaskController {

    private final ITaskService taskService;

    private final IProjectService projectService;

    @Autowired
    public TaskController(@NotNull final ITaskService taskService,
                          @NotNull final IProjectService projectService) {
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @PostMapping("/create")
    public String create() throws Exception {
        final Task task = new Task();
        task.setName("Task" + System.currentTimeMillis());
        taskService.add(task);
        return "redirect:/tasks";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") final String id) throws Exception {
        taskService.removeById(id);
        return "redirect:/tasks";
    }

    @GetMapping("/update/{id}")
    public ModelAndView update(@PathVariable("id") final String id) throws Exception {
        final Task task = taskService.findOneById(id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-update");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", projectService.findAll());
        modelAndView.addObject("statusValues", Status.values());
        return modelAndView;
    }

    @PostMapping("/update/{id}")
    public String update(@ModelAttribute("task") Task task) throws Exception {
        taskService.updateById(task.getId(), task.getName(), task.getDescription());
        return "redirect:/tasks";
    }

}
