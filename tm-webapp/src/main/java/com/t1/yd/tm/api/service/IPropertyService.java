package com.t1.yd.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull String getDbSqlDialect();

    String getDbShowSqlFlg();

    @NotNull String getDbStrategy();

    @NotNull
    String getDbServer();

    @NotNull
    String getDbUsername();

    @NotNull
    String getDbPassword();

    @NotNull
    String getDbDriver();

    @NotNull String getDbCacheUseSecondLevel();

    @NotNull String getDbCacheUseQuery();

    @NotNull String getDbCacheUseMinPuts();

    @NotNull String getDbCacheRegionPrefix();

    @NotNull String getDbCacheProviderConfig();

    @NotNull String getDbCacheFactoryClass();

}
