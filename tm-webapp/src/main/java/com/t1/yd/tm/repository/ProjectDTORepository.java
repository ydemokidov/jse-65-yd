package com.t1.yd.tm.repository;

import com.t1.yd.tm.dto.model.ProjectDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectDTORepository extends JpaRepository<ProjectDTO, String> {
}
