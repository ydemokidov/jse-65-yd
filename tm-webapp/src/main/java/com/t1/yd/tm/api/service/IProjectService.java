package com.t1.yd.tm.api.service;

import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectService {

    @NotNull
    Project add(@NotNull Project project) throws Exception;

    void clear() throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @NotNull
    List<Project> findAll() throws Exception;

    @Nullable
    Project findOneById(@Nullable String id) throws Exception;

    int count() throws Exception;

    void remove(@Nullable Project project) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    void update(@Nullable Project project) throws Exception;

    void changeProjectStatusById(@Nullable String id, @Nullable Status status) throws Exception;

    @NotNull
    Project create(@Nullable String name) throws Exception;

    @NotNull
    Project create(@Nullable String name, @Nullable String description) throws Exception;

    void updateById(@Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

}
