package com.t1.yd.tm.repository;

import com.t1.yd.tm.dto.model.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskDTORepository extends JpaRepository<TaskDTO, String> {

    @NotNull
    List<TaskDTO> findByProjectId(@NotNull final String projectId);

}
