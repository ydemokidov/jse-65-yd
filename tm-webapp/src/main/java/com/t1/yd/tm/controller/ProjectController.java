package com.t1.yd.tm.controller;

import com.t1.yd.tm.api.service.IProjectService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/project")
public class ProjectController {

    private final IProjectService projectService;

    @Autowired
    public ProjectController(@NotNull final IProjectService projectService) {
        this.projectService = projectService;
    }

    @PostMapping("/create")
    public String create() throws Exception {
        final Project project = new Project();
        project.setName("Project" + System.currentTimeMillis());
        projectService.add(project);
        return "redirect:/projects";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") final String id) throws Exception {
        projectService.removeById(id);
        return "redirect:/projects";
    }

    @GetMapping("/update/{id}")
    public ModelAndView update(@PathVariable("id") final String id) throws Exception {
        final Project project = projectService.findOneById((id));
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-update");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statusValues", Status.values());
        return modelAndView;
    }

    @PostMapping("/update/{id}")
    public String update(@ModelAttribute("project") Project project, BindingResult result) throws Exception {
        projectService.updateById(project.getId(), project.getName(), project.getDescription());
        return "redirect:/projects";
    }

}
