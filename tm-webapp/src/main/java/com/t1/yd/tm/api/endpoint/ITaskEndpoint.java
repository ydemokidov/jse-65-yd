package com.t1.yd.tm.api.endpoint;

import com.t1.yd.tm.dto.model.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskEndpoint {

    @NotNull
    List<TaskDTO> findAll() throws Exception;

    @NotNull
    TaskDTO save(@NotNull TaskDTO task) throws Exception;

    @Nullable
    TaskDTO findById(@NotNull String id) throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    long count() throws Exception;

    void deleteById(@NotNull String id) throws Exception;

    void delete(@NotNull TaskDTO task) throws Exception;

    void clear() throws Exception;


}
