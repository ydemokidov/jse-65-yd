package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.service.IProjectDTOService;
import com.t1.yd.tm.dto.model.ProjectDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/project")
public class RestProjectEndpoint implements IProjectEndpoint {

    private final IProjectDTOService projectDTOService;

    @Autowired
    public RestProjectEndpoint(IProjectDTOService projectDTOService) {
        this.projectDTOService = projectDTOService;
    }

    @Override
    @GetMapping("/list")
    public @NotNull List<ProjectDTO> findAll() throws Exception {
        return projectDTOService.findAll();
    }

    @Override
    @PostMapping("/update")
    public @NotNull ProjectDTO save(@RequestBody @NotNull final ProjectDTO project) throws Exception {
        return projectDTOService.update(project);
    }

    @Override
    @GetMapping("/{id}")
    public @Nullable ProjectDTO findById(@PathVariable @NotNull final String id) throws Exception {
        return projectDTOService.findOneById(id);
    }

    @Override
    @GetMapping("/exists/{id}")
    public boolean existsById(@PathVariable @NotNull final String id) throws Exception {
        return projectDTOService.existsById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() throws Exception {
        return projectDTOService.count();
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable @NotNull final String id) throws Exception {
        projectDTOService.removeById(id);
    }

    @Override
    @DeleteMapping("/delete")
    public void delete(@RequestBody @NotNull final ProjectDTO project) throws Exception {
        projectDTOService.remove(project);
    }

    @Override
    public void clear() throws Exception {
        projectDTOService.clear();
    }

}
