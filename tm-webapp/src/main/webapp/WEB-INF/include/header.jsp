<html>
<head>
    <style>

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        button {
            border: none;
            color: white;
            background-color: #008CBA;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }

    </style>
</head>
<body>
<table>
    <tr>
        <td style="width: 20%; background-color: #008CBA; color: white;">
            <h2 style="margin-bottom: 0;">YD Task Manager</h2>
        </td>
        <td style="text-align: right;">
            <a href="/projects">PROJECTS</a> | <a href="/tasks">TASKS</a>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-top: 20px;">
