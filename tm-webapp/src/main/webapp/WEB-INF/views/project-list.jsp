<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/header.jsp"/>

<h3>PROJECT LIST</h3>

<table>
    <tr style="background-color: #f9dca2;">
        <th style="width: 25%;">ID</th>
        <th style="width: 20%;">Name</th>
        <th style="width: 20%;">Description</th>
        <th style="width: 10%;">Status</th>
        <th style="width: 10%;">Created</th>
        <th style="width: 5%;">Edit</th>
        <th style="width: 10%;">Delete</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            </td>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td>
                <c:out value="${project.status.displayName}"/>
            </td>
            <td>
                <fmt:parseDate value="${project.created}" pattern="yyyy-MM-dd" var="projectCreatedStr" type="date"/>
                <fmt:formatDate pattern="yyyy-MM-dd" value="${projectCreatedStr}"/>
            </td>
            <td>
                <a href="/project/update/${project.id}"/>EDIT</a>
            </td>
            <td>
                <a href="/project/delete/${project.id}"/>DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/project/create" style="padding-top: 20px;">
    <button>CREATE PROJECT</button>
</form>

<jsp:include page="../include/footer.jsp"/>
