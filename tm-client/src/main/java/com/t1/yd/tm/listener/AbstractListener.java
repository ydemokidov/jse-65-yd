package com.t1.yd.tm.listener;

import com.t1.yd.tm.api.model.IListener;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.event.ConsoleEvent;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public abstract class AbstractListener implements IListener {

    @NotNull
    protected final ITokenService tokenService;

    public AbstractListener(@NotNull final ITokenService tokenService) {
        this.tokenService = tokenService;
    }

    public abstract void handle(@NotNull final ConsoleEvent consoleEvent);

    @NotNull
    public abstract String getName();

    @Nullable
    public abstract String getArgument();

    @Override
    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    public abstract String getDescription();

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    public void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();

        @NotNull String result = "";

        if (!name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (!description.isEmpty()) result += description;
        return result;
    }

}
