package com.t1.yd.tm.listener.system;

import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.event.ConsoleEvent;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationAboutListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "about";
    @NotNull
    public static final String ARGUMENT = "-a";
    @NotNull
    public static final String DESCRIPTION = "Show info about author";

    @Autowired
    public ApplicationAboutListener(@NotNull final ITokenService tokenService,
                                    @NotNull final IPropertyService propertyService) {
        super(tokenService, propertyService);
    }

    @Override
    @EventListener(condition = "@applicationAboutListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ABOUT]");
        System.out.println("Author: " + getPropertyService().getAuthorName());
        System.out.println("Email: " + getPropertyService().getAuthorEmail());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
