package com.t1.yd.tm.listener.system;

import com.t1.yd.tm.api.model.IListener;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.listener.AbstractListener;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Setter
public class CommandListListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "commands";
    @NotNull
    public static final String ARGUMENT = "-comm";
    @NotNull
    public static final String DESCRIPTION = "Show available commands";

    @NotNull
    private List<AbstractListener> listeners;

    @Autowired
    public CommandListListener(@NotNull final ITokenService tokenService,
                               @NotNull final IPropertyService propertyService) {
        super(tokenService, propertyService);
    }

    @Override
    @EventListener(condition = "@commandListListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMMANDS]");

        for (@NotNull final String commandName : getCommandNamesList()) {
            System.out.println(commandName);
        }

    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    public List<String> getCommandNamesList() {
        List<String> commandNames = new ArrayList<>();

        for (@NotNull final IListener listener : listeners) {
            @NotNull final String name = listener.getName();
            if (name.isEmpty()) continue;
            commandNames.add(listener.getName());
        }

        return commandNames;
    }

}
