package com.t1.yd.tm;

import com.t1.yd.tm.component.Bootstrap;
import com.t1.yd.tm.configuration.ClientConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public final class TaskManagerClient {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ClientConfiguration.class);
        final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}
