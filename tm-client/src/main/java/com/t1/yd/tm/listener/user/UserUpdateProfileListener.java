package com.t1.yd.tm.listener.user;

import com.t1.yd.tm.api.endpoint.IAuthEndpoint;
import com.t1.yd.tm.api.endpoint.IUserEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.user.UserUpdateProfileRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class UserUpdateProfileListener extends AbstractUserListener {

    @NotNull
    private final String name = "user_update_profile";
    @NotNull
    private final String description = "Update user profile";

    @Autowired
    public UserUpdateProfileListener(@NotNull final ITokenService tokenService,
                                     @NotNull final IUserEndpoint userEndpointClient,
                                     @NotNull final IAuthEndpoint authEndpointClient) {
        super(tokenService, userEndpointClient, authEndpointClient);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @EventListener(condition = "@userUpdateProfileListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[UPDATE USER PROFILE]");

        System.out.println("[ENTER LAST NAME:]");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("[ENTER FIRST NAME:]");
        @NotNull final String fstName = TerminalUtil.nextLine();
        System.out.println("[ENTER MID NAME:]");
        @NotNull final String midName = TerminalUtil.nextLine();

        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest();
        request.setFirstName(fstName);
        request.setLastName(lastName);
        request.setMidName(midName);
        request.setToken(getToken());

        getUserEndpoint().updateProfile(request);

        System.out.println("[USER PROFILE UPDATED]");
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public String getDescription() {
        return description;
    }

}
