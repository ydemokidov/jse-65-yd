package com.t1.yd.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ITokenService {

    @Nullable
    String getToken();

    void setToken(String token);

}
