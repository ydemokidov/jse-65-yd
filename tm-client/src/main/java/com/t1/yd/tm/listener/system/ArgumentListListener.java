package com.t1.yd.tm.listener.system;

import com.t1.yd.tm.api.model.IListener;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.listener.AbstractListener;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Component
public class ArgumentListListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "arguments";
    @NotNull
    public static final String ARGUMENT = "-arg";
    @NotNull
    public static final String DESCRIPTION = "Show available arguments";

    @NotNull
    private List<AbstractListener> listeners;

    @Autowired
    public ArgumentListListener(@NotNull final ITokenService tokenService,
                                @NotNull final IPropertyService propertyService) {
        super(tokenService, propertyService);
    }

    @Override
    @EventListener(condition = "@argumentListListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ARGUMENTS]");

        for (final String arg : getArgumentList()) {
            System.out.println(arg);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    public List<String> getArgumentList() {
        @NotNull final List<String> args = new ArrayList<>();

        @NotNull final Collection<AbstractListener> listenersWithArgument = listeners.stream()
                .filter(abstractListener -> abstractListener.getArgument() != null)
                .collect(Collectors.toList());

        for (@NotNull final IListener listener : listenersWithArgument) {
            @Nullable final String arg = listener.getArgument();
            if (arg == null || arg.isEmpty()) continue;
            args.add(arg);
        }

        return args;
    }

}
