package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.dto.IProjectDtoService;
import com.t1.yd.tm.api.service.dto.IUserDtoService;
import com.t1.yd.tm.configuration.ServerConfiguration;
import com.t1.yd.tm.enumerated.Status;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.t1.yd.tm.constant.ProjectTestData.*;
import static com.t1.yd.tm.constant.UserTestData.*;

@Tag("com.t1.yd.tm.marker.UnitCategory")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ServerConfiguration.class})
public class ProjectDTOServiceTest {

    @Autowired
    private IUserDtoService userDtoService;

    @Autowired
    private IProjectDtoService service;

    @BeforeEach
    public void initRepository() {
        service.clear();
        userDtoService.clear();
        userDtoService.set(ALL_USER_DTOS);
    }

    @Test
    public void add() {
        service.add(USER_1_PROJECT_DTO_1);
        Assertions.assertEquals(USER_1_PROJECT_DTO_1.getId(), service.findOneById(USER_1_PROJECT_DTO_1.getId()).getId());
    }

    @Test
    public void addWithUser() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        Assertions.assertEquals(USER_1_PROJECT_DTO_1.getId(), service.findOneById(USER_1_PROJECT_DTO_1.getId()).getId());
    }

    @Test
    public void addAll() {
        service.add(ALL_PROJECT_DTOS);
        Assertions.assertEquals(ALL_PROJECT_DTOS.size(), service.getSize());
        Assertions.assertEquals(USER_1_PROJECT_DTO_2.getId(), service.findOneById(USER_1_PROJECT_DTO_2.getId()).getId());
    }

    @Test
    public void removeById() {
        service.add(ALL_PROJECT_DTOS);
        service.removeById(USER_1_PROJECT_DTO_2.getId());
        Assertions.assertEquals(ALL_PROJECT_DTOS.size() - 1, service.getSize());
        Assertions.assertNull(service.findOneById(USER_1_PROJECT_DTO_2.getId()));
    }


    @Test
    public void clear() {
        service.add(ALL_PROJECT_DTOS);
        service.clear();
        Assertions.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_2);
        service.add(ADMIN.getId(), ADMIN_PROJECT_DTO_1);
        Assertions.assertEquals(USER_1_PROJECT_DTOS.size(), service.findAll(USER_DTO_1.getId()).size());
    }

    @Test
    public void findOneByIdWithUserId() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_2);
        service.add(ADMIN.getId(), ADMIN_PROJECT_DTO_1);

        Assertions.assertEquals(USER_1_PROJECT_DTO_1.getId(), service.findOneById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).getId());
        Assertions.assertNull(service.findOneById(ADMIN.getId(), USER_1_PROJECT_DTO_1.getId()));
    }

    @Test
    public void existsById() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        Assertions.assertTrue(service.existsById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()));
        Assertions.assertFalse(service.existsById(ADMIN.getId(), USER_1_PROJECT_DTO_2.getId()));
    }

    @Test
    public void updateById() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        service.updateById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId(), PROJECT_NAME, PROJECT_DESCRIPTION);
        Assertions.assertEquals(PROJECT_NAME, service.findProjectById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).getName());
        Assertions.assertEquals(PROJECT_DESCRIPTION, service.findProjectById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).getDescription());
    }

    @Test
    public void changeStatusById() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        service.changeStatusById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId(), Status.COMPLETED);
        Assertions.assertEquals(Status.COMPLETED, service.findProjectById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).getStatus());
    }

    @AfterEach
    public void clearData() {
        service.clear();
        userDtoService.clear();
    }

}
