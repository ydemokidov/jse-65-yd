package com.t1.yd.tm.api.service.model;

import com.t1.yd.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface IUserOwnedService<E extends AbstractEntity> extends IService<E> {

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Sort sort);

    void clear(@NotNull String userId);

    E add(@NotNull String userId, @NotNull E entity);

    @NotNull
    List<E> findAll(@NotNull String userId);

    @Nullable
    E findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    E removeById(@NotNull String userId, @NotNull String id);

    boolean existsById(@NotNull String userId, @NotNull String id);

    long getSize(@NotNull String userId);

}
