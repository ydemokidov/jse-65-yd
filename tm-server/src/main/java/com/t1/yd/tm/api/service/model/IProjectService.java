package com.t1.yd.tm.api.service.model;

import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.NotNull;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    Project updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @NotNull
    Project changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

}
