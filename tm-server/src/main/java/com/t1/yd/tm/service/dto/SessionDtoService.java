package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.dto.ISessionDtoService;
import com.t1.yd.tm.dto.model.SessionDTO;
import com.t1.yd.tm.repository.dto.SessionDtoJpaRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Collection;

@Service
public class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, SessionDtoJpaRepository> implements ISessionDtoService {

    private final SessionDtoJpaRepository repository;

    @Autowired
    public SessionDtoService(@NotNull final ILoggerService loggerService,
                             @NotNull final SessionDtoJpaRepository repository) {
        super(loggerService);
        this.repository = repository;
    }

    @NotNull
    @Override
    protected SessionDtoJpaRepository getRepository() {
        return repository;
    }


    @Override
    public @NotNull SessionDTO update(@NotNull SessionDTO entity) {
        throw new NotImplementedException();
    }

    @Override
    @Transactional
    public SessionDTO add(@NotNull String userId, @NotNull SessionDTO entity) {
        return super.add(userId, entity);
    }

    @Override
    @Transactional
    public void clear(@NotNull String userId) {
        super.clear(userId);
    }

    @Nullable
    @Override
    @Transactional
    public SessionDTO removeById(@NotNull String userId, @NotNull String id) {
        return super.removeById(userId, id);
    }


    @NotNull
    @Override
    @Transactional
    public SessionDTO add(@NotNull SessionDTO entity) {
        return super.add(entity);
    }

    @Override
    @Transactional
    public void clear() {
        super.clear();
    }

    @Nullable
    @Override
    @Transactional
    public SessionDTO removeById(@NotNull String id) {
        return super.removeById(id);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<SessionDTO> set(@NotNull Collection<SessionDTO> collection) {
        return super.set(collection);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<SessionDTO> add(@NotNull Collection<SessionDTO> collection) {
        return super.add(collection);
    }

}
