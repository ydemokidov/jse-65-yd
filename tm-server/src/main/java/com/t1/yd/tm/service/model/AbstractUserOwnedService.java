package com.t1.yd.tm.service.model;

import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.model.IUserOwnedService;
import com.t1.yd.tm.api.service.model.IUserService;
import com.t1.yd.tm.exception.entity.EntityNotFoundException;
import com.t1.yd.tm.exception.entity.UserNotFoundException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.UserIdEmptyException;
import com.t1.yd.tm.model.AbstractUserOwnedEntity;
import com.t1.yd.tm.model.User;
import com.t1.yd.tm.repository.model.AbstractJpaUserOwnedRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;

import java.util.List;

public abstract class AbstractUserOwnedService<E extends AbstractUserOwnedEntity, R extends AbstractJpaUserOwnedRepository<E>> extends AbstractService<E, R> implements IUserOwnedService<E> {

    protected final IUserService userService;

    public AbstractUserOwnedService(@NotNull final ILoggerService loggerService,
                                    @NotNull final IUserService userService) {
        super(loggerService);
        this.userService = userService;
    }

    @Override
    public E add(@NotNull final String userId, @NotNull final E entity) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        final User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        entity.setUser(user);
        getRepository().save(entity);
        return entity;
    }

    @Override
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        getRepository().deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return getRepository().findAllByUserId(userId, sort);
    }

    @Nullable
    @Override
    public E findOneById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findByUserIdAndId(userId, id).orElse(null);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final E result = findOneById(userId, id);
        if (result == null) throw new EntityNotFoundException();
        getRepository().deleteByUserIdAndId(userId, id);
        return result;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        return getRepository().existsByUserIdAndId(userId, id);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().count();
    }

}
