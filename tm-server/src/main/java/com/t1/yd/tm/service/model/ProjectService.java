package com.t1.yd.tm.service.model;

import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.model.IProjectService;
import com.t1.yd.tm.api.service.model.IUserService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.repository.model.ProjectJpaRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProjectService extends AbstractUserOwnedService<Project, ProjectJpaRepository> implements IProjectService {

    private final ProjectJpaRepository repository;

    @Autowired
    public ProjectService(@NotNull final ILoggerService loggerService,
                          @NotNull final ProjectJpaRepository repository,
                          @NotNull final IUserService userService) {
        super(loggerService, userService);
        this.repository = repository;
    }

    @NotNull
    @Override
    protected ProjectJpaRepository getRepository() {
        return repository;
    }

    @Override
    @Transactional
    public @NotNull Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(userId, project);
    }

    @Override
    @Transactional
    public @NotNull Project updateById(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description) {
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return update(project);
    }

    @Override
    @Transactional
    public @NotNull Project changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return update(project);
    }

    @Override
    @Transactional
    public Project add(@NotNull String userId, @NotNull Project entity) {
        return super.add(userId, entity);
    }

    @Override
    @Transactional
    public void clear(@NotNull String userId) {
        super.clear(userId);
    }

    @Nullable
    @Override
    @Transactional
    public Project removeById(@NotNull String userId, @NotNull String id) {
        return super.removeById(userId, id);
    }

}
