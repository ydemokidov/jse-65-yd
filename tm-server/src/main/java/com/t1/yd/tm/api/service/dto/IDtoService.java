package com.t1.yd.tm.api.service.dto;

import com.t1.yd.tm.dto.model.AbstractEntityDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;

import java.util.Collection;
import java.util.List;

public interface IDtoService<E extends AbstractEntityDTO> {

    void clear();

    @NotNull List<E> findAll();

    @NotNull List<E> findAll(@Nullable Sort sort);

    @NotNull E add(@NotNull E entity);

    @NotNull E update(@NotNull E entity);

    @NotNull Collection<E> add(@NotNull Collection<E> entities);

    @NotNull Collection<E> set(@NotNull Collection<E> entities);

    boolean existsById(@NotNull String id);

    E findOneById(@NotNull String id);

    E removeById(@NotNull String id);

    long getSize();

}
