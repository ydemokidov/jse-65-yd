package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.service.IAuthService;
import com.t1.yd.tm.dto.model.SessionDTO;
import com.t1.yd.tm.dto.request.AbstractUserRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.user.AccessDeniedException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@NoArgsConstructor
public class AbstractEndpoint {

    @NotNull
    protected IAuthService authService;

    public AbstractEndpoint(@NotNull final IAuthService authService) {
        this.authService = authService;
    }

    protected SessionDTO check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @Nullable final SessionDTO sessionDTO = authService.validateToken(token);
        @Nullable final Role userRole = sessionDTO.getRole();
        if (userRole == null) throw new AccessDeniedException();
        final boolean check = userRole.equals(role);
        if (!check) throw new AccessDeniedException();
        return sessionDTO;
    }

    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        return authService.validateToken(token);
    }


}
