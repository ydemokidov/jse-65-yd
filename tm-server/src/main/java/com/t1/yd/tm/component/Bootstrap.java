package com.t1.yd.tm.component;

import com.t1.yd.tm.api.endpoint.*;
import com.t1.yd.tm.api.service.*;
import com.t1.yd.tm.api.service.dto.*;
import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.util.SystemUtil;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService;


    @Getter
    @NotNull
    private final ILoggerService loggerService;

    @Getter
    @NotNull
    private final IProjectDtoService projectService;

    @Getter
    @NotNull
    private final ITaskDtoService taskService;

    @Getter
    @NotNull
    private final IDtoProjectTaskService projectTaskService;

    @Getter
    @NotNull
    private final IUserDtoService userService;

    @NotNull
    private final ISessionDtoService sessionService;

    @Getter
    @NotNull
    private final IAuthService authService;

    @Getter
    @NotNull
    private final IDomainService domainService;

    @NotNull
    private final Backup backup;

    @NotNull
    private final ISystemEndpoint systemEndpoint;

    @NotNull
    private final IUserEndpoint userEndpoint;

    @NotNull
    private final IProjectEndpoint projectEndpoint;

    @NotNull
    private final ITaskEndpoint taskEndpoint;

    @NotNull
    private final IDataEndpoint dataEndpoint;

    @NotNull
    private final IAuthEndpoint authEndpoint;

    @Autowired
    public Bootstrap(@NotNull final IPropertyService propertyService,
                     @NotNull final ILoggerService loggerService,
                     @NotNull final IProjectDtoService projectService,
                     @NotNull final ITaskDtoService taskService,
                     @NotNull final IDtoProjectTaskService projectTaskService,
                     @NotNull final IUserDtoService userService,
                     @NotNull final ISessionDtoService sessionService,
                     @NotNull final IAuthService authService,
                     @NotNull final IDomainService domainService,
                     @NotNull final Backup backup,
                     @NotNull final ISystemEndpoint systemEndpoint,
                     @NotNull final IUserEndpoint userEndpoint,
                     @NotNull final IProjectEndpoint projectEndpoint,
                     @NotNull final ITaskEndpoint taskEndpoint,
                     @NotNull final IDataEndpoint dataEndpoint,
                     @NotNull final IAuthEndpoint authEndpoint) {
        this.propertyService = propertyService;
        this.loggerService = loggerService;
        this.projectService = projectService;
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
        this.userService = userService;
        this.sessionService = sessionService;
        this.authService = authService;
        this.domainService = domainService;
        this.backup = backup;
        this.systemEndpoint = systemEndpoint;
        this.userEndpoint = userEndpoint;
        this.projectEndpoint = projectEndpoint;
        this.taskEndpoint = taskEndpoint;
        this.dataEndpoint = dataEndpoint;
        this.authEndpoint = authEndpoint;
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);

    }

    public void run() {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(dataEndpoint);
        initDemoData();
        initLogger();
        initPID();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void initDemoData() {
        userService.create("admin", "admin", "admin@mail.ru", Role.ADMIN);
        userService.create("user1", "user1", "user1@mail.ru");
        userService.create("user2", "user2", "user2@mail.ru");

        String user1id = userService.findByLogin("user1").getId();
        String user2id = userService.findByLogin("user2").getId();

        ProjectDTO project1 = projectService.add(user1id, new ProjectDTO(user1id, "project1", "my first project"));
        projectService.add(user1id, new ProjectDTO(user1id, "project2", "my 2nd project"));
        projectService.add(user1id, new ProjectDTO(user1id, "project3", "my 3rd project"));
        ProjectDTO project4 = projectService.add(user2id, new ProjectDTO(user2id, "project4", "my 4th project"));
        projectService.add(user2id, new ProjectDTO(user2id, "project5", "my 5th project"));

        TaskDTO task1 = taskService.add(user1id, new TaskDTO(user1id, "task1", "my 1st task"));
        taskService.add(user1id, new TaskDTO(user1id, "task2", "my 2nd task"));
        TaskDTO task3 = taskService.add(user2id, new TaskDTO(user2id, "task3", "my 3rd task"));
        taskService.add(user2id, new TaskDTO(user2id, "task4", "my 4th task"));

        projectTaskService.bindTaskToProject(user1id, task1.getId(), project1.getId());
        projectTaskService.bindTaskToProject(user2id, task3.getId(), project4.getId());
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initLogger() {
        loggerService.initJmsLogger();
        loggerService.info("** WELCOME TO TASK MANAGER **");
    }

    private void prepareShutdown() {
        backup.stop();
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
    }

    @Override
    public @NotNull ISessionDtoService getSessionService() {
        return sessionService;
    }

}