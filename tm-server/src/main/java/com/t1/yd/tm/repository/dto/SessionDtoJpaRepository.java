package com.t1.yd.tm.repository.dto;

import com.t1.yd.tm.dto.model.SessionDTO;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

@Repository
@Scope("prototype")
public interface SessionDtoJpaRepository extends AbstractDtoUserOwnedJpaRepository<SessionDTO> {

}
