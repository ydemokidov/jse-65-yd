package com.t1.yd.tm.repository.dto;

import com.t1.yd.tm.dto.model.TaskDTO;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskDtoJpaRepository extends AbstractDtoUserOwnedJpaRepository<TaskDTO> {

    List<TaskDTO> findAllByUserIdAndProjectId(String userId, String projectId);

    List<TaskDTO> findAllByProjectId(String projectId);

}
