package com.t1.yd.tm.service.model;

import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.model.ITaskService;
import com.t1.yd.tm.api.service.model.IUserService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.repository.model.TaskJpaRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TaskService extends AbstractUserOwnedService<Task, TaskJpaRepository> implements ITaskService {

    private final TaskJpaRepository repository;

    @Autowired
    public TaskService(@NotNull final ILoggerService loggerService,
                       @NotNull final IUserService userService,
                       @NotNull final TaskJpaRepository repository) {
        super(loggerService, userService);
        this.repository = repository;
    }

    @Override
    @Transactional
    public @NotNull Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(userId, task);
    }

    @Override
    @Transactional
    public @NotNull Task changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return update(task);
    }

    @Override
    public @NotNull List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return repository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @NotNull
    @Override
    protected TaskJpaRepository getRepository() {
        return repository;
    }

    @Override
    @Transactional
    public Task add(@NotNull String userId, @NotNull Task entity) {
        return super.add(userId, entity);
    }

    @Override
    @Transactional
    public void clear(@NotNull String userId) {
        super.clear(userId);
    }

    @Nullable
    @Override
    @Transactional
    public Task removeById(@NotNull String userId, @NotNull String id) {
        return super.removeById(userId, id);
    }

}
