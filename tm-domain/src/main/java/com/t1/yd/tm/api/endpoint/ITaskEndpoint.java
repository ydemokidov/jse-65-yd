package com.t1.yd.tm.api.endpoint;

import com.t1.yd.tm.dto.request.task.*;
import com.t1.yd.tm.dto.response.task.*;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ITaskEndpoint extends IEndpoint {

    @NotNull
    String NAME = "TaskEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull IConnectionProvider connectionProvider) {
        @NotNull final String host = connectionProvider.getHost();
        @NotNull final String port = connectionProvider.getPort();
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ITaskEndpoint.class);
    }

    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ITaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskBindToProjectResponse bindTaskToProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskBindToProjectRequest request);

    @NotNull
    @WebMethod
    TaskUnbindFromProjectResponse unbindTaskFromProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskUnbindFromProjectRequest request);

    @NotNull
    @WebMethod
    TaskChangeStatusByIdResponse changeTaskStatusById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskChangeStatusByIdRequest request);

    @NotNull
    @WebMethod
    TaskClearResponse clearTasks(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskClearRequest request);

    @NotNull
    @WebMethod
    TaskCompleteByIdResponse completeTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskCompleteByIdRequest request);

    @NotNull
    @WebMethod
    TaskCreateResponse createTask(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskCreateRequest request);

    @NotNull
    @WebMethod
    TaskListByProjectIdResponse listTasksByProjectId(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskListByProjectIdRequest request);

    @NotNull
    @WebMethod
    TaskListResponse listTasks(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskListRequest request);

    @NotNull
    @WebMethod
    TaskRemoveByIdResponse removeTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskRemoveByIdRequest request);

    @NotNull
    @WebMethod
    TaskShowByIdResponse showTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskShowByIdRequest request);

    @NotNull
    @WebMethod
    TaskStartByIdResponse startTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskStartByIdRequest request);

    @NotNull
    @WebMethod
    TaskUpdateByIdResponse updateTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskUpdateByIdRequest request);

}