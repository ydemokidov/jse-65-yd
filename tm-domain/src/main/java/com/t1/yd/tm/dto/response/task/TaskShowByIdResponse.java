package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.dto.model.TaskDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class TaskShowByIdResponse extends AbstractTaskResponse {

    public TaskShowByIdResponse(@Nullable final TaskDTO taskDTO) {
        super(taskDTO);
    }

    public TaskShowByIdResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
