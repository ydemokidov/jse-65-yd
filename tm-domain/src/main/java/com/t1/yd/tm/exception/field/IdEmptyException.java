package com.t1.yd.tm.exception.field;

public class IdEmptyException extends AbstractFieldException {

    public IdEmptyException() {
        super("Error! Id is empty...");
    }

}
