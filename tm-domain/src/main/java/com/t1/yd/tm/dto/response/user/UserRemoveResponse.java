package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.dto.model.UserDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse(@Nullable final UserDTO userDTO) {
        super(userDTO);
    }

    public UserRemoveResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
