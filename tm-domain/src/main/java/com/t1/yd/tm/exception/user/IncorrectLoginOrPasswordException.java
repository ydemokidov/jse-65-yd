package com.t1.yd.tm.exception.user;

public class IncorrectLoginOrPasswordException extends AbstractUserException {

    public IncorrectLoginOrPasswordException() {
        super("Error! Incorrect login or password...");
    }

}
