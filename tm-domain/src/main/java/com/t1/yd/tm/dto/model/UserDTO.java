package com.t1.yd.tm.dto.model;

import com.t1.yd.tm.enumerated.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Cacheable
@Table(name = "users")
@NoArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDTO extends AbstractEntityDTO {

    @NotNull
    @Column(length = 150, nullable = false)
    private String login;

    @NotNull
    @Column(name = "pwd_hash", length = 500, nullable = false)
    private String passwordHash;

    @NotNull
    @Column(length = 100, nullable = false)
    private String email;

    @Nullable
    @Column(name = "fst_name", length = 100)
    private String firstName;

    @Nullable
    @Column(name = "last_name", length = 100)
    private String lastName;

    @Nullable
    @Column(name = "mid_name", length = 100)
    private String middleName;

    @NotNull
    @Column(length = 50, nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(nullable = false)
    private Boolean locked = false;

}